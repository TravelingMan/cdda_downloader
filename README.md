# Cataclysm DDA Updater

###OUT OF DATE
Will be updating when time allows, but this project is dead. Will rewrite it with
better practices. Not useful at the moment.

The Cataclysm DDA Updater is a simple program for downloading and installing the most recent
version of Cataclysm DDA. It should be considered primitive and untested. In fact, you shouldn't
use it at all right now. It's fragile, held together with duct-tape, and might actually shave your
cat bald.

## Usage
Downloads get installed into D:\Games\CDDA because I haven't made it configurable yet. To run:

```
CataExperimental
```

If it works, the program will download the newest zip file of the game, backup your current game,
extract the new game, and move your saves and configuration settings over from your old game.

### Configuration Options

None yet. Did I mention this was primitive?

### Roadmap

What's here now works well for me, but it's a learning experience for C# more than an attempt at
anything extraordinary (or even useful for anyone else). I had an itch. Coming soon, toddler
allowing:

* Configuration!
* Unit tests
* More defensive coding practices

#### License
GNU GPL v2.0; see LICENSE.
