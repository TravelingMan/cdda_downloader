﻿using System;
using System.IO;
using System.IO.Compression;

namespace CataExperimental
{
    class FileManager
    {
        /// <summary>
        /// Backup the game directory, extract the update into a new folder, copy saves from old version to new.
        /// </summary>
        public void ProcessUpdate()
        {
            Program.ColorOut("Comitting update!\n", ConsoleColor.Cyan);

            BackupGameDirectory();
            if (true)
            {
                Extract(LocationStore.downloadedFile.Name, 
                    LocationStore.gameLocation.FullName + Path.GetFileNameWithoutExtension(LocationStore.downloadedFile.Name));
                CopySaves();
            }
        }
        /// <summary>
        /// Backs up the existing game folder. Wise to use this before the Update() method.
        /// </summary>
        /// <remarks>
        /// Uses the Visual Basic .NET assembly to produce a well-engineered Directory Copy
        /// function. May need to be refactored. Uncertain on overhead. "Stop writing code that
        /// already exists in the framework!" Additionally, the engineers that built the VB.Net
        /// function I'm using here are a lot smarter than me.
        /// </remarks>
        private void BackupGameDirectory()
        {
            // Remove old backups to clean any possible leftover trash from earlier CDDA builds
            if (Directory.Exists(LocationStore.backupLocation.FullName))
            {
                Directory.Delete(LocationStore.backupLocation.FullName, true);
            }

            Console.Write("Backing up old version ... ",
                LocationStore.currentVersion.FullName,
                LocationStore.backupLocation.FullName);
            Microsoft.VisualBasic.FileIO.FileSystem.CopyDirectory(
                LocationStore.currentVersion.FullName, LocationStore.backupLocation.FullName, true);
            Program.ColorOut("Done.\n", ConsoleColor.Green);
        }

        private void CopySaves()
        {
            var newFolderName = LocationStore.gameLocation.FullName + Path.GetFileNameWithoutExtension(LocationStore.downloadedFile.Name);
            
            if (LocationStore.currentVersion.FullName != newFolderName)
            {
                Microsoft.VisualBasic.FileIO.FileSystem.CopyDirectory(
                LocationStore.currentVersion.FullName + @"\save", newFolderName + @"\save");

                // Copy settings
                Microsoft.VisualBasic.FileIO.FileSystem.CopyDirectory(
                    LocationStore.currentVersion.FullName + @"\config", newFolderName + @"\config");
            }
        }

        /// <summary>
        /// Extracts a zip archive into a folder inside the gameLocation directory.
        /// </summary>
        /// <param name="source">Location of the downloaded archive, assumes it is in the downloadLocation</param>
        /// <param name="destination">Folder to extract the archive into, assumes it goes into the gameLocation</param>
        private void Extract(string source, string destination)
        {
            if (Directory.Exists(destination))
            {
                Program.ColorOut("Latest version appears to be installed already.\n", ConsoleColor.Yellow);
            }
            else
            {
                Console.Write("Extracting files ... ");                
                ZipFile.ExtractToDirectory(LocationStore.downloadLocation.FullName + source, destination);
                Program.ColorOut("Done.\n", ConsoleColor.Green);
            }
        }

        
    }
}
