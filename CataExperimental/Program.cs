﻿using System;



namespace CataExperimental
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string version = System.Reflection.Assembly.GetExecutingAssembly()
                                           .GetName()
                                           .Version
                                           .ToString();

            Console.WriteLine("Cataclysm DDA Updater, version {0}", version);

            GameManager.CheckForUpdates();

            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }

        internal static void ColorOut (string text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.Write(text);
            Console.ResetColor();
        }
    }
}