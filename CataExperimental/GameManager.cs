﻿using System;
using System.IO;
using System.IO.Compression;

namespace CataExperimental
{
    /// <summary>
    /// Manages the game updates via the included static methods.
    /// </summary>
    internal class GameManager
    {
        public static void CheckForUpdates()
        {
            LocationStore.currentVersion = GetCurrentVersion();
            WebCrawler webCrawler = new WebCrawler();
        }

        /// <summary>
        /// Searches the gameFolder for a list of directories matching the "cdda*" pattern.
        /// </summary>
        /// <returns>The FileInfo object of the most recent game folder.</returns>
        public static DirectoryInfo GetCurrentVersion()
        {
            string[] dirs = Directory.GetDirectories(LocationStore.gameLocation.FullName, "cataclysmdda*", SearchOption.TopDirectoryOnly);
            int lastIndex = dirs.Length - 1; // May implement natural sorting later

            // TODO Handle the empty directory scenario better.
            if (lastIndex < 0)
            {
                Program.ColorOut("No existing game version detected.\n", ConsoleColor.Yellow);

                Directory.CreateDirectory(LocationStore.gameLocation.FullName + "cataclysmdda-empty");
                return new DirectoryInfo(LocationStore.gameLocation.FullName + "cataclysmdda-empty");
            }
            else
            {
                //return new DirectoryInfo(LocationStore.gameLocation.FullName + dirs[lastIndex]);
                return new DirectoryInfo(dirs[lastIndex]);
            }
        }

        /// <summary>
        /// Update the game folder by overwriting the current latest version of the game (to
        /// preserve saves and
        /// settings) . The older version is preserved as a copy.
        /// </summary>
        public static void CommitUpdate()
        {
            // TODO FileManager to handle update, backup
            FileManager fileMan = new FileManager();
            fileMan.ProcessUpdate();
                        
            Console.WriteLine("Update complete. Enjoy Cataclysm!");
        }
    }
}