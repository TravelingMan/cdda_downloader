﻿using System.IO;


namespace CataExperimental
{
    /// <summary>
    /// Stores important folder and file locations for use throughout the program.
    /// </summary>
    class LocationStore
    {
        public static DirectoryInfo gameLocation = new DirectoryInfo(@"D:\Games\CDDA\");
        public static DirectoryInfo downloadLocation = new DirectoryInfo(@"D:\Games\CDDA\downloads\");
        public static DirectoryInfo backupLocation = new DirectoryInfo(@"D:\Games\CDDA\backup\");

        public static DirectoryInfo currentVersion;
        public static FileInfo downloadedFile;
    }
}
