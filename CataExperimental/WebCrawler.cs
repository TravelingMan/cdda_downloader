﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace CataExperimental
{
    internal struct LinkItem
    {
        public string Href;
        public string Text;

        public override string ToString()
        {
            return "Href: " + Href + "\nText: " + Text;
        }
    }

    /// <summary>
    /// Access the CDDA website to get and parse the download page. Provide download links for the
    /// latest version, then retrieve the most recent zip archive.
    /// </summary>
    internal class WebCrawler
    {
        private string htmlContent; // Content of the page, populated by GetPage()
        private string url = @"http://dev.narc.ro/cataclysm/jenkins-latest/Windows/Tiles/";
        private List<LinkItem> linkList; // Links (and links only) retrieved by ParsePage()        

        public WebCrawler()
        {            
            Initialize();

            if (UpdateAvailable())
            {
                DownloadLatest(LocationStore.downloadLocation.FullName);
                GameManager.CommitUpdate();
            }
            else
            {
                Program.ColorOut("No update available. Already at latest version.\n", ConsoleColor.Green);
            }
        }

        private void Initialize()
        {
            GetPage();
            linkList = ParsePage(htmlContent);
            if (linkList.Count != 0)
            {                
                linkList = ParsePage(htmlContent);
            }
            else
            {
                Program.ColorOut("Error(s) prevented successful execution.\n", ConsoleColor.Red);
            }
        }

        private bool UpdateAvailable()
        {
            // True if the last zip file is not the same as the current folder name                        
            if (linkList.Last().Href != LocationStore.currentVersion.FullName + ".zip")
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// For debugging; writes the captured links to the console.
        /// </summary>        
        private void DisplayLinks()
        {
            foreach (var item in linkList)
            {
                Console.WriteLine(item + System.Environment.NewLine);
            }
        }

        /// <summary>
        /// Access the download page and retrieve the most recent build archive.
        /// </summary>
        private void DownloadLatest(string downloadFolder)
        {
            string fileName = linkList.Last().Href;
            string fileUrl = url + fileName;
            string destination = downloadFolder + fileName;

            if (File.Exists(destination))
            {
                Console.WriteLine("Newest file, {0}, already downloaded.", fileName);
                LocationStore.downloadedFile = new FileInfo(fileName);
            }
            else
            {
                Console.Write("Downloading {0} ... ", fileName);

                using (WebClient _webClient = new WebClient())
                {
                    _webClient.DownloadFile(fileUrl, destination);
                }

                LocationStore.downloadedFile = new FileInfo(fileName);
                Program.ColorOut("Done.\n", ConsoleColor.Green);
            }
        }        

        /// <summary>
        /// Access the url and downloads the page markup for later parsing via ParsePage().
        /// </summary>
        private void GetPage()
        {
            Console.Write("Looking for updates ... ", url);
            using (WebClient _webClient = new WebClient())
            {
                try
                {
                    this.htmlContent = _webClient.DownloadString(url);
                }
                catch (WebException e)
                {
                    Console.WriteLine("A web exception occured : {0}", e.Message);
                }
            }

            Program.ColorOut("Done.\n", ConsoleColor.Green);
        }

        /// <summary>
        /// Parses HTML markup and gets hyperlinks, then stores them into a List of LinkItems.
        /// </summary>
        /// <param name="page">The HTML content to parse</param>
        /// <returns></returns>
        private List<LinkItem> ParsePage(string page)
        {
            page = htmlContent;
            List<LinkItem> list = new List<LinkItem>();

            MatchCollection matchHtml = Regex.Matches(page, @"(<a.*?>.*?</a>)",
                RegexOptions.Singleline);

            foreach (Match matchItem in matchHtml)
            {
                string value = matchItem.Groups[1].Value;
                LinkItem i = new LinkItem();

                // Set the Href attribute
                Match match = Regex.Match(value, @"href=\""(.*?)\""",
                    RegexOptions.Singleline);

                if (match.Success)
                {
                    i.Href = match.Groups[1].Value;
                }

                // Set the Text attribute
                string t = Regex.Replace(value, @"\s*</*?>\s*", "",
                    RegexOptions.Singleline);
                i.Text = t;

                list.Add(i);
            }
            return list;
        }
    }
}